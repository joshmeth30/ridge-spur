<?php
/**
 * Template Name: Homepage
 */
?>

<?php // while (have_posts()) : the_post(); ?>
  <?php //get_template_part('templates/page', 'header'); ?>
  <?php //get_template_part('templates/content', 'page'); ?>
<?php // endwhile; ?>

<?php get_template_part('templates/who_we_are'); ?>

<!-- Services Section -->
<?php get_template_part('templates/services'); ?>

<!-- Portfolio Grid Section -->
<?php get_template_part('templates/portfolio'); ?>

<!-- Testimonials Aside -->
<?php get_template_part('templates/testimonials'); ?>

<!-- About Section -->
<?php get_template_part('templates/blog'); ?>

<!-- Team Section -->
<?php get_template_part('templates/team'); ?>

<!-- Clients Aside -->
<?php get_template_part('templates/twitter' ); ?>

<!-- Clients Aside -->
<?php // get_template_part('templates/clients' ); ?>

<!-- Portfolio Modals -->
<?php get_template_part('templates/portfolio-modals'); ?>

    
    


