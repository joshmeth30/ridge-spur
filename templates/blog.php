<section id="about">
    
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading brand-ribbon-left">Our Blog</h2>
            <!-- <h3 class="section-subheading text-muted">We think a lot. So we figured we'd share.</h3> -->
        </div>
    </div>
    <div class="container">
        <div class="row">
        	<div class="col-lg-12">
            	<ul class="timeline">
				<?php $counter = 1; ?>
				<?php query_posts( array ('posts_per_page' => 4 ) ); //'category_name' => 'blog' ?> 
				<?php while (have_posts()) : the_post(); ?>
					
					<?php 
					$counter++;
		        	if ($counter % 2 == 0) {
		         	$verter = 'timeline-inverted';
		        	} else { $verter = ''; }
		        	?>
		        	<?php 
					if ( has_post_thumbnail() ) {
						$thumb_id = get_post_thumbnail_id();
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail');
						$thumb_url = $thumb_url_array[0];
					} 
					?>

	

					<li class="<?= $verter ?>">
	                    <a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">	
	                    	<div class="timeline-image">
	                        	<img class="img-thumbnail img-responsive" src="<?= $thumb_url; ?>" alt="">
	                    	</div>
	                    </a>
	                    <div class="timeline-panel">
	                        <div class="timeline-heading">
	                            
	                            <h4 class="subheading"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
	                            <h5><?php the_time('F jS, Y'); ?></h5>
	                        </div>
	                        <div class="timeline-body">
	                            <p class="text-muted"><?php the_excerpt(); ?></p>
	                        </div>
	                    </div>
	                </li>


					<?php endwhile; ?>
					<?php wp_reset_query(); ?>

					<li class="text-center" style="min-height:0px;">
	                    <a href="/our-blog" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
	                        <button class="btn btn-primary btn-lg uppercase" alt="">See All Blogs</button> 
	                    </a>
	                </li>
				</ul>
				
            </div>
		</div>
    </div>
</section>