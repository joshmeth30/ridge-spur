<aside class="clients">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <!-- <a href="#"> -->
                    <img src="<?= get_template_directory_uri() . '/dist/images/logos/drrice-logo.png'; ?>" class="img-responsive img-centered" alt="" style="padding-top: 45px;">
                <!-- </a> -->
            </div>
            <div class="col-md-3 col-sm-6">
                <!-- <a href="#"> -->
                    <img src="<?= get_template_directory_uri() . '/dist/images/case-studies/globalwrap.png'; ?>" class="img-responsive img-centered" alt="">
                <!-- </a> -->
            </div>
            <div class="col-md-3 col-sm-6">
                <!-- <a href="#"> -->
                    <img src="<?= get_template_directory_uri() . '/dist/images/logos/kidney-featured.png'; ?>" class="img-responsive img-centered" alt="">
                    
                <!-- </a> -->
            </div>
            <div class="col-md-3 col-sm-6">
                <!-- <a href="#"> -->
                    <img src="<?= get_template_directory_uri() . '/dist/images/logos/zingo-featured.png'; ?>" class="img-responsive img-centered" alt="">
                <!-- </a> -->
            </div>
        </div>
    </div>
</aside>