<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-primary">Work With Us</h2>
                <h4 class="section-subheading brand-secondary"><a href="mailto:info@ridgespur.com"><i class="fa fa-envelope-o"></i> info@ridgespur.com</a></h4>
                <h4 class="section-subheading brand-secondary"><a href="tel:2678389494"><i class="fa fa-phone"></i> 267-838-9494</a></h4>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?= do_shortcode('[contact-form-7 id="27" title="Contact form 1"]' ); ?>
            </div>
        </div>
    </div>
</section>