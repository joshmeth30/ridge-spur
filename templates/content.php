<article <?php post_class('pad-top'); ?>>
	<header class="pad-bot">
		<h2 class="entry-title brand-secondary"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>	
	</header>
	
	<div class="col-lg-2">
		<?php 
		if ( has_post_thumbnail() ) {
			$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail');
			$thumb_url = $thumb_url_array[0];
		} 
		?>
	 	<div class="services-img img-rounded" style="background-image: url('<?= $thumb_url; ?>');">	</div>
	</div>
	
	<div class="col-lg-10">
		<div class="entry-summary">
			<?php get_template_part('templates/entry-meta'); ?>
			<p></p>
			<?php the_excerpt(); ?>

		</div>
	</div>

</article>
<div class="clearfix"></div>
