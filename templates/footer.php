<!-- Contact Section -->
<?php 
$footer_contact = get_field('footer_contact');

if ($footer_contact == 1 || is_front_page() ) {
    get_template_part('templates/contact-us'); 
    # code...
}
// if ( !is_page('landscape') ) {
//     get_template_part('templates/contact-us'); 
// } else return;

?>


<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->



<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?= get_template_directory_uri() . '/js/classie.js'; ?>"></script>
<script src="<?= get_template_directory_uri() . '/js/cbpAnimatedHeader.js'; ?>"></script>


<!-- Custom Theme JavaScript -->
<script src="<?= get_template_directory_uri() . '/js/agency.js'; ?>"></script>

<script src="<?= get_template_directory_uri() . '/js/smoothscroll.js'; ?>"></script>





<footer class="content-info" role="contentinfo">
	
	<div class="container">
		<?php dynamic_sidebar('sidebar-footer'); ?>
	</div>

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright">Ridge Spur Media <?php echo date("Y") ?></span>
            </div>
            <div class="col-md-4">
                <ul class="list-inline social-buttons">
                    <li><a href="https://twitter.com/ridgespur"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="http://ridgespur.com/access-case-study/"><i class="fa fa-facebook"></i></a></li>
                    <!-- <li><a href="#"><i class="fa fa-linkedin"></i></a></li> -->
                </ul>
            </div>
            <div class="col-md-4">
                <!-- <ul class="list-inline quicklinks">
                    <li><a href="<?= esc_url(home_url('/')) . 'sitemap_index.xml'; ?>">Site Map</a>
                    </li>
                </ul> -->
                <?php
                  if (has_nav_menu('footer_navigation')) :
                    wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'list-inline quicklinks']);
                  endif;
                ?>
            </div>
        </div>
    </div>
</footer>