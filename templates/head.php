<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?= get_template_directory_uri() . '/assets/images/favicon.ico'; ?>" />
  <?php wp_head(); ?>
  <meta property="og:image" content="http://ridgespur.com/wp-content/uploads/2016/03/Be-Captivating-Logo.jpg" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="<?= get_template_directory_uri() . '/js/parallax.min.js' ?>"></script>
</head>
