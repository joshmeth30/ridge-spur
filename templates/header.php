<?php 
$text_style = get_field( "intro_text_styling" );
$client_name = get_field( "client_name" );
$client_type = get_field( "client_type" );
$client_url = get_field( "client_url" );


if ( $text_style == 0 ) {
?>
    <style type="text/css">
        .home .intro-lead-in,
        .home .intro-heading {
            background-color: inherit;
            opacity: 1;
        }
    </style>
<?php
}

if ( has_post_thumbnail() ) {
    $thumb_id = get_post_thumbnail_id();
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
    $thumb_url = $thumb_url_array[0];
} else {
    $thumb_url = get_template_directory_uri() . '/dist/images/subway2.jpg';
}
?>

<header>
    <div class="banner" role="banner" data-parallax="scroll" data-image-src="<?= $thumb_url; ?>">
    <div class="container">
        <div class="intro-text">
            <h1>Be Captivating</h1>
            <h2><div class="intro-heading"><?php if ( is_home() ) { echo 'Our Blog'; } else { echo the_title(); } ?></div></h2>
           <!--  <?php if (is_front_page()) {
                echo '<a href="#contact" class="page-scroll btn btn-xl btn-danger">Contact Us</a>';
            }?> -->
        </div>
        <?php if (is_front_page()) {
            echo '<p class="text-right watermark white">' . $client_name . '<br>' . $client_type . '<br>';
            if ($client_url != null ) {
                echo '<a href="' . $client_url . '">See Site</a>';
            }
            echo '</p>';
        }?> 
    </div>
    </div>
  <div class="container">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="<?= esc_url(home_url('/')); ?>">
                    <img class="logo hidden-xs" id="logo-img" src="<?= get_template_directory_uri() . '/dist/images/logo8.png'; ?>">
                    <img class="logo hidden-md hidden-lg hidden-custom" id="logo-img" src="<?= get_template_directory_uri() . '/dist/images/logo7.png'; ?>">
                </a>
            </div>

            <script type="text/javascript">
  
              jQuery(document).ready(function($){
                $(window).scroll(function(){
                    var waypointHeight = 300;
                    if ($('#wpadminbar').length > 0){
                        waypointHeight += $('#wpadminbar').outerHeight();
                    }
                    if($(this).scrollTop() > waypointHeight) {
                        setTimeout(function(){
                            $('#logo-img').attr('src',"<?php echo get_stylesheet_directory_uri().'/dist/images/logo7.png';?>");
                        },200);
                        // $('.banner').css({ 'background-color' : 'rgba(255, 255, 255, 0.9)'});
                    }
                    if($(this).scrollTop() < waypointHeight) {
                        setTimeout(function(){
                            $('#logo-img').attr('src',"<?php echo get_stylesheet_directory_uri().'/dist/images/logo8.png';?>");
                        },200);
                        // $('.banner').css({ 'background-color' : 'transparent'}); 
                    }
                });
              });

            </script>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php
                  if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav navbar-right']);
                  endif;
                  ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    
  </div>
</header>
