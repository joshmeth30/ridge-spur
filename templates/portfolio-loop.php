<?php
$args = array( 'post_type' => 'portfolio', 'posts_per_page' => 100 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();

    if ( has_post_thumbnail() ) {
        $thumb_id = get_post_thumbnail_id();
        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large');
        $thumb_url = $thumb_url_array[0];
    } 
// $i = 7;
    $i = get_field( "counter" );
?>

<!-- Portfolio Looop -->

<div class="col-md-4 col-sm-6 portfolio-item">
    <a href="#portfolioModal<?= $i; ?>" class="portfolio-link" data-toggle="modal">
        <div class="portfolio-hover">
            <div class="portfolio-hover-content">
                <i class="fa fa-plus fa-3x"></i>
            </div>
        </div>
        
        <img src="<?= $thumb_url ?>" class="img-responsive img-portfolio" alt="">
    </a>
    <div class="portfolio-caption">
        <h4><?= the_title(); ?></h4>
        <!-- <p class="text-muted"><?= the_excerpt(); ?></p> -->
    </div>
</div>


<?php 
// $i++;
// wp_reset_postdata();
endwhile; ?>
