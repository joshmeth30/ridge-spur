<?php
$args = array( 'post_type' => 'portfolio', 'posts_per_page' => 100 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();

    if ( has_post_thumbnail() ) {
        $thumb_id = get_post_thumbnail_id();
        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large');
        $thumb_url = $thumb_url_array[0];
    } 
// $i = 7;
    $i = get_field( "counter" );
?>


<!-- Portfolio Modals Looop -->

<div class="portfolio-modal modal fade" id="portfolioModal<?= $i; ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2><?= the_title(); ?></h2>
                        <p class="item-intro text-muted"><?= the_excerpt(); ?></p>
                        <?= the_content(); ?>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
// $i++;
// wp_reset_postdata();
endwhile; ?>
