<!-- Portfolio Modal 1 -->
<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class=""> <!-- col-lg-8 col-lg-offset-2 -->
                    <div class="modal-body">                  
                        <?php query_posts( array ('pagename' => 'access-case-study' ) ); //'category_name' => 'blog' ?> 
                            <?php while (have_posts()) : the_post(); ?>
                              <?php get_template_part('templates/page', 'header'); ?>
                              <?php get_template_part('templates/content', 'page'); ?>
                            <?php endwhile; ?>
                        <?php wp_reset_query(); ?>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                        <p></p>
                        <p><a href="http://movingcompaniesforward.com">This project was completed in collaboration with Philadelphia-based branding firm, Muhlenhaupt + Company.​</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 6 -->
<div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <h1 class="text-center text-blue">A Digital Marketing Case Study</h1>
                        <h2>Background</h2>
                        <p class="lead">Steven Rice, DDS is a dentist who runs a multi-doctor office located in Philadelphia. His office has been in in business for over 40 years and is a well established practice.  Using the most advanced state of the art equipment, Dr. Rice and associates provide top results in a friendly atmosphere.</p>
                        <p class="lead">In 2013, Dr. Rice approached My Patient Growth for more information regarding building a modern website as well as finding out more information on digital marketing.  After learning about the strategies used in acquiring new patients, Dr. Rice signed up for web development, SEO and reputation management.</p>
                        <h2>Approach</h2>
                        <p class="lead">Dr. Rice was interesting in doing things right the first time.  My Patient Growth built his practice a new website from the ground up.  The site development focused on ease of navigate with a modern feel.  The site was also made to showcase the expertise of the practice.  Building a new site also allowed for SEO optimization.</p>
                        <p class="lead">Driving brand recognition and promoting Dr. Rice&#8217;s reputation, My Patient Growth listed his practice with a number of directories including Yelp and Dr.OOgle.</p>
                        <p class="lead">Re-engaging previous patients was also a priority.  My Patient Growth created newsletter blasts to former patients that had not been back for a defined period of time, providing a friendly reminder to schedule an appointment.</p>
                        <h2>Annual Results</h2>
                        <p class="lead"><b>81</b> Patient Website Inquires</p>
                        <p class="lead"><b>30%</b> (24) Converted to Scheduled Appointments</p>
                        <hr class="featurette-divider" />
                        <p class="lead"><b>24</b> New Patients from Website Inquires</p>
                        <p class="lead"><b>72</b> New Patients from Directories Listings</p>
                        <hr class="featurette-divider" />
                        <p class="lead"><b>96</b> Total Patients Resulting from Digital Marketing Techniques</p>
                        <p>&nbsp;</p>
                        <h2>Value</h2>
                        <p class="lead"><b>96</b> New Patients <b>X</b> <u>Avg. Lifetime Customer Value of a Patient is $15,000</u></p>
                        <p class="lead"><u>Total Revenue</u> of <b>$1,440,000</b></p>
                        <hr class="featurette-divider" />
                        <p class="lead text-blue"><b>A $200 return on investment for every $1 spent in marketing with My Patient Growth</b></p>
                        <p class="lead text-blue">By understanding the value of investing in a new website along with other forms of digital marketing, Dr. Steven Rice gained an additional 96 new patients to his practice in a one year period.  He continues to use My Patient Growth to expand his practice and has signed on for additional services after seeing the success of previous campaigns.</p>
                        <p class="lead text-blue">My Patient Growth was able to provide him the professional services he was looking for along with a strong return on investment allowing Dr. Rice to focus on what matters most, making smiling faces.</p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modals Loop -->
<?php get_template_part('templates/portfolio-modals-loop'); ?>

