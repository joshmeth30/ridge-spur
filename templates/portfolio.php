<section id="portfolio" class="bg-light-gray">
    
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading brand-ribbon-right">Portfolio</h2>
            <!-- <h3 class="section-subheading text-muted">Case Studies and Showcases.</h3> -->
        </div>
    </div>
    <div class="container">
        <div class="row">
            

            <div class="col-md-4 col-sm-6 portfolio-item">
                <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img src="<?= get_template_directory_uri() . '/dist/images/case-studies/access-logo2.jpg'; ?>" class="img-responsive img-portfolio" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>Access</h4>
                    <!-- <p class="text-muted">Lead Generation System</p> -->
                </div>
            </div>
            
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img src="<?= get_template_directory_uri() . '/dist/images/case-studies/dr-rice.png'; ?>" class="img-responsive img-portfolio" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>Dr. Rice Dentist</h4>
                    <!-- <p class="text-muted">Lead Generation System</p> -->
                </div>
            </div>
            
            <!-- Portfolio Modals Loop -->
            <?php get_template_part('templates/portfolio-loop'); ?>
            
        </div>
    </div>
</section>