<section id="services">
    <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="brand-ribbon-left section-heading">Services</h2>
                <!-- <div class="col-lg-6 col-lg-offset-3">
                    <h3 class="section-subheading text-muted" style="line-height:1.38;">From website design to social media management and everything in between, you need professionals to make your company look professional. You need Ridge Spur.</h3>
                </div> -->
            </div>
        </div>
    <div class="container">

        
    

        <?php

        $args = array( 'post_type' => 'service', 'posts_per_page' => 20 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();

            if ( has_post_thumbnail() ) {
                $thumb_id = get_post_thumbnail_id();
                $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail');
                $thumb_url = $thumb_url_array[0];
            } 
        ?>
            <div class="col-lg-4 col-sm-6 text-center pad-bot-2">
                <a href="<?= the_permalink(); ?>">
                <div class="services-img img-thumbnail img-centered" style="background-image: url('<?= $thumb_url; ?>');">    </div>
                <div class="clearfix"></div>
                <!-- <hr> -->
                <h3><?= the_title(); ?></a></h3>
                <!-- <p><?= the_excerpt(); ?></p> -->
            
            </div>

        <?php endwhile; ?>


        
    </div>
</section>