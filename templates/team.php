<section id="team" class="bg-light-gray">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading brand-ribbon-right">Team</h2>
        </div>
    </div>
    <div class="container">    
        <!-- <div class="col-sm-4">
            <div class="team-member"> 
                <img class="tyler img-responsive img-thumbnail" src="<?= get_template_directory_uri() . '/dist/images/team/tyler4.jpg'; ?>" alt="">
                <h4>Tyler Caton</h4>
                <script type="text/javascript">
                    jQuery(function(){
                         $(".tyler").hover(
                              function(){this.src = this.src.replace("tyler4","tyler-alter3");},
                              function(){this.src = this.src.replace("tyler-alter3","tyler4");
                         });
                    });       
                </script>
                <p class="text-muted">Managing Partner</p>

            </div>
        </div>

        <div class="col-sm-4">
            <div class="team-member">
                <img src="<?= get_template_directory_uri() . '/dist/images/team/josh.png'; ?>" class="josh img-responsive img-thumbnail" alt="">
                <h4>Josh Meth</h4>
                <script type="text/javascript">
                    jQuery(function(){
                         $(".josh").hover(
                              function(){this.src = this.src.replace("josh","kayak88");},
                              function(){this.src = this.src.replace("kayak88","josh");
                         });
                    });       
                </script>
                <p class="text-muted">Managing Partner</p>

            </div>
        </div> -->
    
    <?php
        
        $args = array( 'post_type' => 'team', 'posts_per_page' => 20 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
        
        $alt_img = get_field('alt_pic');
        // $the_id = get_the_id();
        
        if ( has_post_thumbnail() ) {
            $thumb_id = get_post_thumbnail_id();
            $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full');
            $thumb_url = $thumb_url_array[0];
        } 
        // $alt_array[$the_id] = array( 'id' => $the_id, 'og-img' => $thumb_url, 'alt-img' => $alt_img);
        // echo $alt_array['339']['alt-img'];
        ?>
        <div class="col-sm-4">
                <div class="team-member showhim"> 
                    <img class="ok img-responsive img-thumbnail" src="<?= $thumb_url; ?>" alt="">
                    <img class="showme img-responsive img-thumbnail" src="<?= $alt_img; ?>" alt="">
                    <h4><?= the_title(); ?></h4>
                
                    <style type="text/css">
                        .showme{ display: none; }
                        .showhim:hover .showme{ display : block; }
                        .showhim:hover .ok{ display : none; }
                    </style>

                    <p class="text-muted"><?= the_excerpt(); ?></p>
                </div>
            </div>

        <?php endwhile; ?>
    </div>
</section>