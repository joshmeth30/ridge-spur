<?php
$counter_indicator = 0;
$counter = 0;

$args = array( 'post_type' => 'testimonials', 'posts_per_page' => 100 );
$loop = new WP_Query( $args );

?>

	
<div id="carousel-id" class="carousel slide" data-ride="carousel">
	
	<!-- <ol class="carousel-indicators">
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<li data-target="#carousel-id" data-slide-to="<?= $counter_indicator; ?>" class="<?php if($counter_indicator==0){echo "active";} ?>"></li>
			<?php ++$counter_indicator; ?>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</ol> -->

	<div class="carousel-inner">
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="testimonial item <?php if($counter==0){echo "active";} ?>">
				<div class="container">
					<div class="carousel-caption">
						<h3><i class="fa fa-quote-left pad-right" aria-hidden="true"> </i><?= get_the_content(); ?><i class="fa fa-quote-right pad-left" aria-hidden="true"> </i></h3>
						<p><?= get_the_title(); ?></p>
						<p><?= get_the_excerpt(); ?></p>
					</div>
				</div>
			</div>
			<?php ++$counter; ?>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div>

	<a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>

<script type="text/javascript">
	$('.carousel').carousel({
  interval: 1000 * 6
});
</script>