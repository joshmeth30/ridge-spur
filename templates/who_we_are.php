<section id="who_we_are" class="bg-light-gray">
    
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading brand-ribbon-right">Who We Are</h2>
            <!-- <h3 class="section-subheading text-muted">Case Studies and Showcases.</h3> -->
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center pad-bot-2">
                <h4>Ridge Spur Media is the premier lead generation and digital strategy provider in the tri-state area.</h4>
                <p> We work with small and medium size companies to deliver new customers and captivating online presences. 
                Business owners look to Ridge Spur when:</p>
            </div>
            <div class="row text-center pad-bot">
            
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <i class="fa fa-2x fa-bolt text-primary " aria-hidden="true"></i>
                    <br>
                    <p class="pad-top">Seeking a competitive advantage by improving their digital brand</p>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <i class="fa fa-2x fa-pencil brand-secondary" aria-hidden="true"></i>
                    <br>
                    <p class="pad-top">Traditional marketing means have failed to deliver consistent prospects</p> 
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <i class="fa fa-2x fa-bar-chart text-primary " aria-hidden="true"></i>
                    <br>
                    <p class="pad-top">They are frustrated with the lack of transparency and understanding typical marketing companies provide</p>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <i class="fa fa-2x fa-road brand-secondary" aria-hidden="true"></i>
                    <br>
                    <p class="pad-top">They are searching for a truly comprehensive digital strategy for their company</p>
                </div>

            </div>

           
            
        </div>
    </div>
</section>